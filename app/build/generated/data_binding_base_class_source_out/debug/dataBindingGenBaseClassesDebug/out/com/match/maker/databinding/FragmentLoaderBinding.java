package com.match.maker.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.airbnb.lottie.LottieAnimationView;

public abstract class FragmentLoaderBinding extends ViewDataBinding {
  @NonNull
  public final LottieAnimationView animationView;

  protected FragmentLoaderBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, LottieAnimationView animationView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.animationView = animationView;
  }

  @NonNull
  public static FragmentLoaderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentLoaderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentLoaderBinding>inflate(inflater, com.match.maker.R.layout.fragment_loader, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentLoaderBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static FragmentLoaderBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<FragmentLoaderBinding>inflate(inflater, com.match.maker.R.layout.fragment_loader, null, false, component);
  }

  public static FragmentLoaderBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static FragmentLoaderBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (FragmentLoaderBinding)bind(component, view, com.match.maker.R.layout.fragment_loader);
  }
}
