package com.match.maker.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

public abstract class ActivityLoginBinding extends ViewDataBinding {
  @NonNull
  public final Button btnSignIn;

  @NonNull
  public final AutoCompleteTextView etPassword;

  @NonNull
  public final AutoCompleteTextView etUserName;

  @NonNull
  public final TextInputLayout tilPassword;

  @NonNull
  public final TextInputLayout tilUserName;

  @NonNull
  public final Toolbar toolbar;

  @NonNull
  public final View view;

  protected ActivityLoginBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, Button btnSignIn, AutoCompleteTextView etPassword,
      AutoCompleteTextView etUserName, TextInputLayout tilPassword, TextInputLayout tilUserName,
      Toolbar toolbar, View view) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnSignIn = btnSignIn;
    this.etPassword = etPassword;
    this.etUserName = etUserName;
    this.tilPassword = tilPassword;
    this.tilUserName = tilUserName;
    this.toolbar = toolbar;
    this.view = view;
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityLoginBinding>inflate(inflater, com.match.maker.R.layout.activity_login, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityLoginBinding>inflate(inflater, com.match.maker.R.layout.activity_login, null, false, component);
  }

  public static ActivityLoginBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityLoginBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityLoginBinding)bind(component, view, com.match.maker.R.layout.activity_login);
  }
}
