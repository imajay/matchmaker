package com.match.maker.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public abstract class ActivityHomeBinding extends ViewDataBinding {
  @NonNull
  public final SwipeRefreshLayout pullToRefresh;

  @NonNull
  public final RecyclerView recyclerView;

  @NonNull
  public final Toolbar toolbar;

  @NonNull
  public final TextView txtNoData;

  protected ActivityHomeBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, SwipeRefreshLayout pullToRefresh, RecyclerView recyclerView,
      Toolbar toolbar, TextView txtNoData) {
    super(_bindingComponent, _root, _localFieldCount);
    this.pullToRefresh = pullToRefresh;
    this.recyclerView = recyclerView;
    this.toolbar = toolbar;
    this.txtNoData = txtNoData;
  }

  @NonNull
  public static ActivityHomeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityHomeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityHomeBinding>inflate(inflater, com.match.maker.R.layout.activity_home, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityHomeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivityHomeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivityHomeBinding>inflate(inflater, com.match.maker.R.layout.activity_home, null, false, component);
  }

  public static ActivityHomeBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivityHomeBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivityHomeBinding)bind(component, view, com.match.maker.R.layout.activity_home);
  }
}
