package com.match.maker.databinding;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import de.hdodenhof.circleimageview.CircleImageView;

public abstract class RecyclerAdapterHomeActivityBinding extends ViewDataBinding {
  @NonNull
  public final ImageButton btnConnect;

  @NonNull
  public final ImageButton btnDecline;

  @NonNull
  public final CardView cardView;

  @NonNull
  public final CircleImageView imgProfile;

  @NonNull
  public final TextView txtCardTime;

  @NonNull
  public final TextView txtUserName;

  @NonNull
  public final TextView txtUserPersonalDescription;

  @NonNull
  public final TextView txtUserProfessionalDescription;

  protected RecyclerAdapterHomeActivityBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, ImageButton btnConnect, ImageButton btnDecline, CardView cardView,
      CircleImageView imgProfile, TextView txtCardTime, TextView txtUserName,
      TextView txtUserPersonalDescription, TextView txtUserProfessionalDescription) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnConnect = btnConnect;
    this.btnDecline = btnDecline;
    this.cardView = cardView;
    this.imgProfile = imgProfile;
    this.txtCardTime = txtCardTime;
    this.txtUserName = txtUserName;
    this.txtUserPersonalDescription = txtUserPersonalDescription;
    this.txtUserProfessionalDescription = txtUserProfessionalDescription;
  }

  @NonNull
  public static RecyclerAdapterHomeActivityBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static RecyclerAdapterHomeActivityBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<RecyclerAdapterHomeActivityBinding>inflate(inflater, com.match.maker.R.layout.recycler_adapter_home_activity, root, attachToRoot, component);
  }

  @NonNull
  public static RecyclerAdapterHomeActivityBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static RecyclerAdapterHomeActivityBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<RecyclerAdapterHomeActivityBinding>inflate(inflater, com.match.maker.R.layout.recycler_adapter_home_activity, null, false, component);
  }

  public static RecyclerAdapterHomeActivityBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static RecyclerAdapterHomeActivityBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (RecyclerAdapterHomeActivityBinding)bind(component, view, com.match.maker.R.layout.recycler_adapter_home_activity);
  }
}
