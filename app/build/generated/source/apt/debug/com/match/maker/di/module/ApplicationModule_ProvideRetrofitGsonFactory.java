// Generated by Dagger (https://google.github.io/dagger).
package com.match.maker.di.module;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class ApplicationModule_ProvideRetrofitGsonFactory implements Factory<Gson> {
  private final ApplicationModule module;

  public ApplicationModule_ProvideRetrofitGsonFactory(ApplicationModule module) {
    this.module = module;
  }

  @Override
  public Gson get() {
    return provideInstance(module);
  }

  public static Gson provideInstance(ApplicationModule module) {
    return proxyProvideRetrofitGson(module);
  }

  public static ApplicationModule_ProvideRetrofitGsonFactory create(ApplicationModule module) {
    return new ApplicationModule_ProvideRetrofitGsonFactory(module);
  }

  public static Gson proxyProvideRetrofitGson(ApplicationModule instance) {
    return Preconditions.checkNotNull(
        instance.provideRetrofitGson(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
