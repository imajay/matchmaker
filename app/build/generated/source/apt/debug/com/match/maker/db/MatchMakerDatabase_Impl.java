package com.match.maker.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import com.match.maker.featureModules.landing.db.MatchingUsersDao;
import com.match.maker.featureModules.landing.db.MatchingUsersDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class MatchMakerDatabase_Impl extends MatchMakerDatabase {
  private volatile MatchingUsersDao _matchingUsersDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `matching_user_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `results` TEXT, `info` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"cd8d7c86d285a828ef91d4b0e234a0c9\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `matching_user_table`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsMatchingUserTable = new HashMap<String, TableInfo.Column>(3);
        _columnsMatchingUserTable.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsMatchingUserTable.put("results", new TableInfo.Column("results", "TEXT", false, 0));
        _columnsMatchingUserTable.put("info", new TableInfo.Column("info", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMatchingUserTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesMatchingUserTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoMatchingUserTable = new TableInfo("matching_user_table", _columnsMatchingUserTable, _foreignKeysMatchingUserTable, _indicesMatchingUserTable);
        final TableInfo _existingMatchingUserTable = TableInfo.read(_db, "matching_user_table");
        if (! _infoMatchingUserTable.equals(_existingMatchingUserTable)) {
          throw new IllegalStateException("Migration didn't properly handle matching_user_table(com.match.maker.db.tables.MatchingUsersTable).\n"
                  + " Expected:\n" + _infoMatchingUserTable + "\n"
                  + " Found:\n" + _existingMatchingUserTable);
        }
      }
    }, "cd8d7c86d285a828ef91d4b0e234a0c9", "95e1ebcefb59a98eb5a1d92685b446e9");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "matching_user_table");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `matching_user_table`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public MatchingUsersDao matchingUsersDao() {
    if (_matchingUsersDao != null) {
      return _matchingUsersDao;
    } else {
      synchronized(this) {
        if(_matchingUsersDao == null) {
          _matchingUsersDao = new MatchingUsersDao_Impl(this);
        }
        return _matchingUsersDao;
      }
    }
  }
}
