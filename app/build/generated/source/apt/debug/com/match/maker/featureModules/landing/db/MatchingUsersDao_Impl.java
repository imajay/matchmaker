package com.match.maker.featureModules.landing.db;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import com.match.maker.db.tables.MatchingUsersTable;
import com.match.maker.featureModules.landing.models.Info;
import com.match.maker.featureModules.landing.models.Result;
import com.match.maker.utils.DataTypeConverter;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class MatchingUsersDao_Impl implements MatchingUsersDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfMatchingUsersTable;

  public MatchingUsersDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMatchingUsersTable = new EntityInsertionAdapter<MatchingUsersTable>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `matching_user_table`(`id`,`results`,`info`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MatchingUsersTable value) {
        stmt.bindLong(1, value.getId());
        final String _tmp;
        _tmp = DataTypeConverter.resultListToString(value.getResults());
        if (_tmp == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, _tmp);
        }
        final String _tmp_1;
        _tmp_1 = DataTypeConverter.InfoToString(value.getInfo());
        if (_tmp_1 == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, _tmp_1);
        }
      }
    };
  }

  @Override
  public void insert(MatchingUsersTable... matchingUsersTables) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMatchingUsersTable.insert(matchingUsersTables);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<MatchingUsersTable> getMatchingUsersData() {
    final String _sql = "SELECT * FROM matching_user_table";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfResults = _cursor.getColumnIndexOrThrow("results");
      final int _cursorIndexOfInfo = _cursor.getColumnIndexOrThrow("info");
      final List<MatchingUsersTable> _result = new ArrayList<MatchingUsersTable>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final MatchingUsersTable _item;
        _item = new MatchingUsersTable();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final List<Result> _tmpResults;
        final String _tmp;
        _tmp = _cursor.getString(_cursorIndexOfResults);
        _tmpResults = DataTypeConverter.stringToResultList(_tmp);
        _item.setResults(_tmpResults);
        final Info _tmpInfo;
        final String _tmp_1;
        _tmp_1 = _cursor.getString(_cursorIndexOfInfo);
        _tmpInfo = DataTypeConverter.stringToInfo(_tmp_1);
        _item.setInfo(_tmpInfo);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Integer getMatchingUsersDataCount() {
    final String _sql = "SELECT COUNT(results) FROM matching_user_table";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final Integer _result;
      if(_cursor.moveToFirst()) {
        final Integer _tmp;
        if (_cursor.isNull(0)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(0);
        }
        _result = _tmp;
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
